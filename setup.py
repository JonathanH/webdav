import setuptools
import os

pwd = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(pwd, 'README.md')) as f:
    readme = f.read()

setuptools.setup(
    name='webdav',
    version='0.0.1-dev',
    author='Jonathan Horn',
    author_email='python@autoit4you.de',
    description='A small WebDAV client library',
    long_description=pwd,
    install_requires=[
        'lxml',
        'requests',
        'python-dateutil'
    ],
    extras_require={
        'dev': [
            'requests-mock',
            'pycodestyle',
            'nose',
            'coverage'
        ]
    },
    keywords=[],
    packages=setuptools.find_packages(),
    classifiers=[],
    entry_points={}
)
