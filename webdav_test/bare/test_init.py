import webdav


def test_init_url_slash():
    bare = webdav.WebDavBare('http://webdav.test')
    assert bare.root_url == 'http://webdav.test'
    assert bare.base_path == ''
    assert bare.http_auth is None


def test_init_url_port_slash():
    bare = webdav.WebDavBare('http://webdav.test:8080')
    assert bare.root_url == 'http://webdav.test:8080'
    assert bare.base_path == ''
    assert bare.http_auth is None


def test_init_url_def_protocol():
    bare = webdav.WebDavBare('webdav.test/')
    assert bare.root_url == 'http://webdav.test'
    assert bare.base_path == ''
    assert bare.http_auth is None


def test_root_url_suburl():
    bare = webdav.WebDavBare('http://webdav.test/testab/')
    assert bare.root_url == 'http://webdav.test/testab'
    assert bare.base_path == '/testab'
    assert bare.http_auth is None


def test_root_url_suburl2():
    bare = webdav.WebDavBare('http://webdav.test/test123')
    assert bare.root_url == 'http://webdav.test/test123'
    assert bare.base_path == '/test123'
    assert bare.http_auth is None
