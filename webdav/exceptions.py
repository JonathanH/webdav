class WebDavError(Exception):
    def __init__(self, message):
        super().__init__(message)


class RequestError(WebDavError):
    def __init__(self, message):
        super().__init__(message)


class ErrorResponse(RequestError):
    def __init__(self, code, error, resource):
        super().__init__('There was an error while doing the request for '
                         'resource {}:\nStatus Code: {:d}\nError: {}'
                         .format(resource, code, error))
        self.code = code
        self.error = error
        self.resource = resource


class NotAuthorized(RequestError):
    def __init__(self, resource):
        super().__init__('Your not authorized for resource {}'
                         .format(resource)
                         )


class ResourceNotFound(RequestError):
    def __init__(self, resource):
        super().__init__('Resource {} was not found'.format(resource))


class UnexpectedServerResponse(RequestError):
    def __init__(self, expected, received, data):
        message = 'Expected status code {:d} but received {:d}' \
                   .format(expected, received)
        super().__init__(message)
        self.data = data


class WebDavResourceCreateError(WebDavError):
    def __init__(self, path):
        super().__init__('Ressource "{}" could not be created'.format(path))
        self.path = path
