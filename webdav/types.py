from collections import namedtuple

PropertyText = namedtuple('PropertyText', ['name', 'value'])
PropertyType = namedtuple('PropertyType', ['name', 'children'])
Response = namedtuple('Response', ['resource', 'data'])
ErrorResponse = namedtuple('ErrorResponse', ['resource', 'error', 'affected'])
