import requests
from requests.exceptions import ConnectionError
from lxml.etree import tostring
from lxml.builder import E
from lxml import etree

import re

from . import exceptions
from . import types

PROPFIND_PROPERTY_VALUES = 0
PROPFIND_ALL_PROPERTIES = 1
PROPFIND_LIST_PROPERTIES = 2
DEPTH_INFINITY = -1

HTTP_MULTI_STATUS = 207
HTTP_UNAUTHORIZED = 401
HTTP_FORBIDDEN = 403
HTTP_NOT_FOUND = 404

UNKNOWN_ERROR = -1


class WebDavBare():
    def __init__(self, root_url, http_auth=None, retries=3):
        if '://' not in root_url:
            root_url = 'http://' + root_url
        if root_url.endswith('/'):
            root_url = root_url[:-1]
        self.root_url = root_url
        self.http_auth = http_auth
        start_i = root_url.index('://') + 3
        if '/' in root_url[start_i:]:
            self.base_path = root_url[root_url.index('/', start_i):]
        else:
            self.base_path = ''
        self.retries = retries

    def request_handler(self, method, suburl, data=None, stream=False, retry=0, headers={}, binary=False):
        try:
            if not binary:
                headers['Content-Type'] = 'application/xml; charset="utf-8"'
                headers['Cache-Control'] = 'no-cache'
                headers['Pragma'] = 'no-cache'
            r = requests.request(method, self.root_url + suburl, auth=self.auth, data=data, stream=stream, headers=headers)

            try:
                xml = self.parse_xml(r.text)
            except:
                xml = None

            resource = self.url_to_resource(suburl)
            if r.status_code not in [HTTP_MULTI_STATUS, HTTP_UNAUTHORIZED]:
                if xml and xml.tag == '{DAV:}error':
                    raise exceptions.ErrorResponse(r.status_code, xml, resource)
                raise exceptions.UnexpectedServerResponse(HTTP_MULTI_STATUS, r.status_code, r.text)
            elif r.status_code == HTTP_UNAUTHORIZED:
                raise exceptions.NotAuthorized(resource)
            elif r.status_code == HTTP_NOT_FOUND:
                raise exceptions.ResourceNotFound(resource)
            return r
        except ConnectionError:
            if retry < self.retries:
                return self.request_handler(method, suburl, data=data, stream=stream, retry=retry+1, headers=headers, binary=binary)
            else:
                raise

    def propfind(self, resource, type=PROPFIND_ALL_PROPERTIES, properties=[], depth=DEPTH_INFINITY):
        if depth not in [0, 1, DEPTH_INFINITY, 'infinity']:
            raise ValueError('Unknown depth specified')
        if depth == DEPTH_INFINITY:
            depth = 'infinity'

        if type == PROPFIND_ALL_PROPERTIES:
            body = tostring(E.propfind(E.allprop(), xmlns='DAV:'), xml_declaration=True, encoding='utf-8').decode('utf-8')
        elif type == PROPFIND_LIST_PROPERTIES:
            body = tostring(E.propfind(E.propname(), xmlns='DAV:'), xml_declaration=True, encoding='utf-8').decode('utf-8')
        elif type == PROPFIND_PROPERTY_VALUES:
            fields = [E(prop) for prop in properties]
            body = tostring(E.propfind(E.prop(*fields), xmlns='DAV:'), xml_declaration=True, encoding='utf-8').decode('utf-8')
        else:
            raise ValueError('Unknown type specified')

        r = self.request_handler('PROPFIND', resource, data=body, headers={'Depth': str(depth)})
        xml = self.parse_xml(r.text)

        returns = []

        for base_element in xml.findall('{DAV:}response'):
            return_element = None
            url = self.url_to_resource(base_element.find('{DAV:}href').text)

            for element in base_element.findall('{DAV:}propstat'):
                status = element.find('{DAV:}status').text
                if '200' not in status:
                    if '401' in status:
                        error = HTTP_UNAUTHORIZED
                    elif '403' in status:
                        error = HTTP_FORBIDDEN
                    elif '404' in status:
                        error = HTTP_NOT_FOUND
                    else:
                        error = UNKNOWN_ERROR
                    returns.append(types.ErrorResponse(
                        resource=url,
                        error=error,
                        affected=self.parse_fields_list(element, None)
                    ))
                    continue

                if type == PROPFIND_LIST_PROPERTIES:
                    return_element = self.parse_fields_list(element, return_element)
                else:
                    return_element = self.parse_values(element, return_element, allowed_fields=properties)

            if return_element:
                returns.append(types.Response(resource=url, data=return_element))
        return returns

    def parse_fields_list(self, element, current):
        fields = [x.tag for x in element.find('{DAV:}prop').getchildren()]

        if not current:
            return fields
        else:
            return current.extend(fields)

    def parse_values(self, element, current, allowed_fields=None):
        if not current:
            current = []

        for e in element.find('{DAV:}prop').getchildren():
            if not allowed_fields or e.tag in allowed_fields:
                current.append(self.to_property_tuple(e))

        return current

    def to_property_tuple(self, element):
        children = []

        if element.text:
            return types.PropertyText(name=element.tag, value=element.text)
        elif element.getchildren():
            for e in element.getchildren():
                children.append(self.to_property_tuple(e))
        return types.PropertyType(name=element.tag, children=children)

    def url_to_resource(self, url):
        if url.startswith(self.root_url):
            return url.replace(self.root_url, '')
        else:
            return url.replace(self.base_path, '')

    def parse_xml(self, data):
        try:
            encoding = re.search(r"<\?xml version=('|\")\d+.\d+('|\") encoding=('|\")(\S+)('|\")\?>", data.split('\n')[0]).group(4)
            parser = etree.XMLParser(encoding=encoding)
            xml = etree.fromstring(data.encode(encoding), parser=parser)
        except AttributeError:
            xml = etree.fromstring(data)
        return xml
