import os

import requests
from lxml import etree
from dateutil import parser

from . import exceptions


class WebDav():
    def __init__(self, root_url, auth=None):
        if '://' not in root_url:
            root_url = 'http://' + root_url
        if not root_url.endswith('/'):
            root_url += '/'

        self.base_path = root_url[root_url.index('/', root_url.index('://')+3):]
        self.root_url = root_url
        self.auth = auth

    def request_handler(self, method, suburl, data=None):
        return requests.request(method, self.root_url + suburl, auth=self.auth, data=data)

    def get_file(self, path):
        r = self.request_handler('GET', path)

        if r.status_code == 200:
            return r.text
        else:
            raise exceptions.WebDavUnexpectedStatusCodeError(200, r.status_code)

    def put_file(self, path, content):
        r = self.request_handler('PUT', path, data=content)

        if r.status_code not in [201, 204]:
            raise exceptions.WebDavUnexpectedStatusCodeError(201, r.status_code)

    def upload_file(self, remote_path, local_path):
        with open(local_path, 'rb') as f:
            self.put_file(remote_path, f.read())

    def upload_directory(self, remote_directory, local_directory):
        if not remote_directory.endswith('/'):
            remote_directory += '/'
        if local_directory.endswith('/'):
            local_directory = local_directory[:-1]

        try:
            remote_content = self.walk(remote_directory)
        except exceptions.WebDavResourceNotFound:
            self.mkdir(remote_directory, create_parents=True)
            self.upload_directory(remote_directory=remote_directory, local_directory=local_directory)
            return

        local_content = list(walk_directory(local_directory))
        to_delete = [file for file in remote_content if file not in local_content]
        to_delete_path = [file[:file.rfind('/')] for file in remote_content
                          if '/' in file and not os.path.isdir(local_directory + '/' + file[:file.rfind('/')])]

        to_delete.extend(to_delete_path)

        for removeable_item in to_delete:
            self.delete(remote_directory + removeable_item)

        paths = calc_required_paths(local_content)

        for path in paths:
            try:
                self.propfind(remote_directory + path)
            except exceptions.WebDavResourceNotFound:
                self.mkdir(remote_directory + path)

        for file in local_content:
            self.upload_file(remote_directory + file, local_directory + '/' + file)

    def propfind(self, path):
        r = self.request_handler('PROPFIND', path)

        if r.status_code not in [207, 404]:
            raise exceptions.WebDavUnexpectedStatusCodeError(207, r.status_code)
        elif r.status_code == 404:
            raise exceptions.WebDavResourceNotFound(path)

        xml = etree.fromstring(r.text)
        base_element = xml.find('{DAV:}response/{DAV:}propstat/{DAV:}prop')

        is_file = not len(list(base_element.find('{DAV:}resourcetype')))

        if is_file:
            return {
                'path': xml.find('{DAV:}response/{DAV:}href').text.replace(self.base_path, ''),
                'is-file': True,
                'last-modified': parser.parse(base_element.find('{DAV:}getlastmodified').text),
                'content-length': int(base_element.find('{DAV:}getcontentlength').text),
                'etag': base_element.find('{DAV:}getetag').text,
                'content-type': base_element.find('{DAV:}getcontenttype').text
            }
        else:
            return {
                'path': xml.find('{DAV:}response/{DAV:}href').text.replace(self.base_path, ''),
                'is-file': False,
                'last-modified': parser.parse(base_element.find('{DAV:}getlastmodified').text),
                'quota-used': int(base_element.find('{DAV:}quota-used-bytes').text),
                'quota-available': int(base_element.find('{DAV:}quota-available-bytes').text),
                'etag': base_element.find('{DAV:}getetag').text
            }

    def ls(self, path='', include_directories=False, include_files=True, use_base_path=False):
        if path and not path.endswith('/'):
            path += '/'

        r = self.request_handler('PROPFIND', path)

        if r.status_code != 207:
            raise exceptions.WebDavUnexpectedStatusCodeError(207, r.status_code)

        xml = etree.fromstring(r.text)

        for element in xml:
            resourcetype = element.find('{DAV:}propstat/{DAV:}prop/{DAV:}resourcetype')
            if resourcetype is None:
                continue
            if not include_directories and len(list(resourcetype)):
                continue
            elif not include_files and not len(list(resourcetype)):
                continue

            href = element.find('{DAV:}href').text.replace(self.base_path, '')
            if not use_base_path:
                href = href.replace(path, '')
            if href and href != path:
                yield href

    def walk(self, path='', include_directories=False, include_files=True, use_base_path=False, root_path=None):
        if path and not path.endswith('/'):
            path += '/'

        if not root_path:
            root_path = path

        r = self.request_handler('PROPFIND', path)

        if r.status_code not in [207, 404]:
            raise exceptions.WebDavUnexpectedStatusCodeError(207, r.status_code)
        elif r.status_code == 404:
            raise exceptions.WebDavResourceNotFound(path)

        xml = etree.fromstring(r.text)
        result = []

        for element in xml:
            href = element.find('{DAV:}href').text.replace(self.base_path, '')
            base_href = href

            if not use_base_path:
                href = href.replace(root_path, '')

            resourcetype = element.find('{DAV:}propstat/{DAV:}prop/{DAV:}resourcetype')
            if resourcetype is None:
                continue
            if not href or base_href == path:
                continue

            if len(list(resourcetype)):
                if include_directories:
                    result.append(href)
                result.extend(self.walk(
                    base_href,
                    include_directories=include_directories,
                    include_files=include_files,
                    use_base_path=use_base_path,
                    root_path=root_path))
            elif include_files:
                result.append(href)
        return result

    def mkdir(self, path, create_parents=False):
        if not path.endswith('/'):
            path += '/'
        r = self.request_handler('MKCOL', path)

        if r.status_code not in [201, 409, 403, 405]:
            raise exceptions.WebDavUnexpectedStatusCodeError(201, r.status_code)
        elif create_parents and r.status_code == 409:
            self.mkdir(path[:path[:-1].rfind('/')+1])
            self.mkdir(path)
        elif r.status_code == 403 or (not create_parents and r.status_code == 409):
            raise exceptions.WebDavResourceCreateError(path)

    def delete(self, path):
        r = self.request_handler('DELETE', path)

        if r.status_code not in [204, 404]:
            raise exceptions.WebDavUnexpectedStatusCodeError(204, r.status_code)


def walk_directory(path):
    for x in os.walk(path):
        for y in x[2]:
            yield (x[0] + '/' + y).replace(path+'/', '')


def calc_required_paths(files):
    directories = []

    for file in files:
        base_index = 0
        while '/' in file[base_index:]:
            base_index = file.index('/', base_index)+1
            if file[:base_index-1] not in directories:
                directories.append(file[:base_index-1])
    return directories